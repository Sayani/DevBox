sudo mkdir /opt/tibco || echo "Tibco home dir exist"
sudo chown vagrant /opt/tibco

currentdir=`pwd`
#cd /vagrant/tibco
#filename=TIB_bwce_2.2.1_linux26gl23_x86_64.zip
#mkdir tmp
#cd tmp
#unzip ../$filename
#./TIBCOUniversalInstaller-lnx-x86-64.bin -silent
#rm -rf *
#filename=TIB_BW_Maven_Plugin_1.2.0.zip
#unzip ../$filename
#echo "/opt/tibco/bwce" | ./install.sh
#rm -rf *
#cd ..
#rm -rf tmp
#cp /vagrant/deploy.sh /opt/tibco/bwce/bwce/2.2/maven/plugins/bw6-maven-plugin
#cd /opt/tibco/bwce/bwce/2.2/maven/plugins/bw6-maven-plugin
#./deploy.sh

grep ContainerTarget /opt/tibco/bwce/studio/4.0/eclipse/configuration/config.ini > /dev/null || echo "ContainerTarget=Docker" >> /opt/tibco/bwce/studio/4.0/eclipse/configuration/config.ini
cp /vagrant/tibco/bwce_cf.zip /opt/tibco/bwce/bwce/2.2/docker/resources/bwce-runtime
cd /opt/tibco/bwce/bwce/2.2/docker
docker build -t tibco/bwce:2.2.1 .
docker build -t tibco/bwce:latest .
cp /vagrant/tibco/mysql-connector-java-5.1.40-bin.jar /opt/tibco/bwce/bwce/2.2/config/drivers/shells/jdbc.mysql.runtime/runtime/plugins/com.tibco.bw.jdbc.datasourcefactory.mysql/lib
cp /vagrant/tibco/ojdbc7.jar /opt/tibco/bwce/bwce/2.2/config/drivers/shells/jdbc.oracle.runtime/runtime/plugins/com.tibco.bw.jdbc.datasourcefactory.oracle/lib

cd $currentdir
